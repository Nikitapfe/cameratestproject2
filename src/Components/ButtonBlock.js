import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';

const ButtonBlock = ({isRecord, onStartVideo, onStopVideo}) => {
    return (
        <View style={styles.buttonBlock}>
            {isRecord ? 
            <TouchableOpacity onPress={onStopVideo} style={styles.stopButton}>
            </TouchableOpacity> :
            <TouchableOpacity onPress={onStartVideo} style={styles.startButton}>
            <View style={styles.redCircle}></View>
        </TouchableOpacity>
        }
        </View>
    );
}

const styles = StyleSheet.create({
    buttonBlock: {
        position: 'absolute', 
        bottom: 30,
        flex: 0, 
        flexDirection: 'row', 
        justifyContent: 'center' 
    },
    startButton: {
      flex: 0,
      backgroundColor: 'red',
      borderRadius: 5,
      width: 40,
      height: 40,
      borderRadius: 25,
      alignSelf: 'center',
      margin: 20,
      padding: 15
    },
    stopButton: {
      flex: 0,
      backgroundColor: '#fff',
      borderRadius: 5,
      width: 40,
      height: 40,
      borderRadius: 10,
      alignSelf: 'center',
      margin: 20,
    },
    redCircle: {
        width: 10, 
        height: 10, 
        borderRadius: 5, 
        backgroundColor: '#000'
    }
});

export default ButtonBlock