import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const PendingView = () => (
    <View style={styles.container}>
      <Text>Waiting</Text>
    </View>
  );

  const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'lightgreen',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
      }
  })

  export default PendingView